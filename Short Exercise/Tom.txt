Skills:

All-round mid-tier knowledge of UE4
 - Especially Useful With:
   - Implementing Animations
   - Socketing Weapons on Characters
   - Blueprints
   - Debugging/Problem Solving

Low-Poly Modelling in Maya
Limited use of Substance Painter


